# ubuntu-rust-test-c

ARG DOCKER_REGISTRY_URL=registry.gitlab.com/docking/
ARG CUSTOM_VERSION=0.1
ARG UBUNTU_VERSION=18.04
ARG RUST_VERSION=1.38.0


FROM ${DOCKER_REGISTRY_URL}ubuntu-rust:${CUSTOM_VERSION}-${UBUNTU_VERSION}-${RUST_VERSION} AS build

WORKDIR /app

COPY ./ /app/

RUN cargo build --release


FROM ${DOCKER_REGISTRY_URL}ubuntu:${CUSTOM_VERSION}-${UBUNTU_VERSION} AS ubuntu-rust-test-c

WORKDIR /app

COPY --from=build /app/target/release/ubuntu-rust-test /app/

CMD /app/ubuntu-rust-test
